import React from 'react';


import { Router, BrowserRouter, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import Rutas from './router/routes';
const history = createBrowserHistory();




class App extends React.Component {

  render() {
    return (
      <Router history={history}>
        <Rutas/>
      </Router>
    );
  }
}



export default App;
