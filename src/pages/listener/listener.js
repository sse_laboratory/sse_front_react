import React from 'react';
import logo from '../logo.svg';
import './sendmessage.css';


import {
  Button,
  //ButtonToolbar,
  Form
} from 'react-bootstrap'



class App extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      eventName: "",
      eventDesc: "",
      event: "66762605",
      messages: ['hello', 'world']
    };

    this.sendMessage = this.sendMessage.bind(this);
  }

  componentWillMount(){
    this.setState({
      source: new EventSource('http://192.168.3.185:7090/stream-flux2?evento=' + this.state.event),
    })
  }

  componentDidMount() {

    const { source, messages } = this.state
    source.onmessage = e => {
      console.log('onmessage');
      console.log(e);
    }
    
    source.addEventListener(this.state.event, e => {
      console.log('this.state.even');
      console.log(e);
      //console.log(messages);
    });
   

  }

  componentWillUnmount() {
    this.this.state.source.removeAllListeners();
    this.this.state.source.close();
  }


  sendMessage() {
    this.setState({ event: this.state.eventName })

    let url = `http://192.168.3.185:7090/save?eventname=${this.state.eventName}&eventdesc=${this.state.eventDesc}&typenoti=n`;
    console.log(url);


    fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'text/event-stream',
        // 'Content-Type': 'aplication/json',
        'Access-Control-Allow-Origin': '*'
      }

    })
      .then(res => res.json())
      .then((data) => {
        console.log(data);

        //this.setState({ event: data })
      })
      .catch(console.log)

  }


 


  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Lista de <code style={{ fontSize: '100%', }}>eventos</code>.
          </p>

          <p>{this.state.eventName}</p>
          <p>{this.state.eventDesc}</p>
          {/* <p>{this.state.messages}</p> */}




          <Form>

            <Form.Group controlId="formBasicEventDesc">
              {/* <Form.Label>Password</Form.Label> */}
              <Form.Control type="text" placeholder="Nombre del evento"
                onChange={(e) => {
                  this.setState({
                    eventName: e.target.value
                  })
                }}
                value={this.state.eventName} />
            </Form.Group>

            <Form.Group controlId="formBasicEventName">
              {/* <Form.Label>Password</Form.Label> */}
              <Form.Control type="text" placeholder="Descipcion del evento"
                onChange={(e) => {
                  this.setState({
                    eventDesc: e.target.value
                  })
                }}
                value={this.state.eventDesc} />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>


            <div>
              <Button variant="primary" size="" block
                onClick={this.sendMessage}>
                Enviar contenido
              </Button>
            </div>






          </Form>

        </div>
      </div>
    );
  }
}



export default App;
