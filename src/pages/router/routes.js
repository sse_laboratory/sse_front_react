


import React from 'react'
import SeeSendMessage from '../sse/sendmessage';
import Listener from '../listener/listener';
import { Switch, Route } from 'react-router-dom';


const routes = [
    {
        path: "/",
        component: SeeSendMessage
    },
    {
        path: "/listener",
        component: Listener
    }
];


function RouteWithSubRoutes(route) {
    return (
        <Route
            path={route.path}
            component={route.component}
        // render={props => (
        //     // pass the sub-routes down to keep nesting
        //     <route.component {...props} routes={route.routes} />
        // )}
        />
    );
}


export default function RouteConfigExample() {
    return (

        <Switch>{
            routes.map((route, i) => (
                <Route key={i} exact path={route.path}
                    component={route.component} />
            ))
        }
        </Switch>

    );
}