import React from 'react';
import logo from '../logo.svg';
import './sendmessage.css';


import {
  Button,
  //ButtonToolbar,
  Form
} from 'react-bootstrap'



class App extends React.Component {


  constructor(props) {
    super(props);
    this.state = {
      eventName: "",
      eventDesc: "",
      event: "holi",
      //source: new EventSource('http://localhost:7090/list2'), 
      source: new EventSource('http://localhost:7090/stream'),
      messages: ['hello', 'world']
    };

    this.sendMessage = this.sendMessage.bind(this);
  }

  componentDidMount() {

    const { source, messages } = this.state
    source.onmessage = e => {
      console.log('onmessage');
      console.log(e);
    }
    
    source.addEventListener('ping', e => {
      console.log('Ping 2');
      console.log(e);
      //console.log(messages);
    });
   

  }

  componentWillUnmount() {
    console.log("Murio la primera interfaz");   
    this.this.state.source.removeAllListeners();
    this.this.state.source.close();
  }

  componentWillUpdate(){
    console.log("componentWillUpdate");
    
  }


  sendMessage() {
    this.setState({ event: this.state.eventName })

    let url = `http://localhost:7090/save?eventname=${this.state.eventName}&eventdesc=${this.state.eventDesc}&typenoti=n`;
    console.log(url);


    fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'text/event-stream',
        // 'Content-Type': 'aplication/json',
        'Access-Control-Allow-Origin': '*'
      }

    })
      .then(res => res.json())
      .then((data) => {
        console.log(data);

        //this.setState({ event: data })
      })
      .catch(console.log)

  }


 


  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Lista de <code style={{ fontSize: '100%', }}>eventos SEE</code>.
          </p>

          <p>{this.state.eventName}</p>
          <p>{this.state.eventDesc}</p>
          {/* <p>{this.state.messages}</p> */}




          <Form>

            <Form.Group controlId="formBasicEventDesc">
              {/* <Form.Label>Password</Form.Label> */}
              <Form.Control type="text" placeholder="Nombre del evento"
                onChange={(e) => {
                  this.setState({
                    eventName: e.target.value
                  })
                }}
                value={this.state.eventName} />
            </Form.Group>

            <Form.Group controlId="formBasicEventName">
              {/* <Form.Label>Password</Form.Label> */}
              <Form.Control type="text" placeholder="Descipcion del evento"
                onChange={(e) => {
                  this.setState({
                    eventDesc: e.target.value
                  })
                }}
                value={this.state.eventDesc} />
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>


            <div>
              <Button variant="primary" size="" block
                onClick={this.sendMessage}>
                Enviar contenido
              </Button>
            </div>






          </Form>

        </div>
      </div>
    );
  }
}



export default App;
